/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
const Bip39 = require('bip39');
//const bitcoinjs = require('bitcoinjs-lib')
//import { Wallet } from 'ibl-core-wallet';
//import Neon from "@cityofzion/neon-js";

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    let privateKey = '';
    //const aa = Neon.create.privateKey();
    //let mnemonic = Bip39.generateMnemonic();
    //const masterSeed = Bip39.mnemonicToSeed(mnemonic);
    //const hdMaster = bitcoinjs.HDNode.fromSeedBuffer(masterSeed, bitcoinjs.networks.bitcoin);
    //const wallet = hdMaster.derivePath("m/44'/888'/0'/0").derive(child);
    //const wif = wallet.keyPair.toWIF();
    //const privateKey = Neon.wallet.getPrivateKeyFromWIF(wif);
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
        <Text style={styles.instructions}>{privateKey}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
